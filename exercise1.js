function countItems(arr, item) {
    let counter = 0;
    for(i=0; i < arr.length; i++) {
      var element = arr[i];
      if(element == item) {
        counter++;
      }
      if(typeof(element)=='object') {
        element.forEach(e => {
          if(e == item) {
            counter++;
          }
        })
      }
    }

    return counter;
}

var arr = [
  "apple",
  ["banana", "strawberry", "apple"]
];
console.log(countItems(arr, "apple"));