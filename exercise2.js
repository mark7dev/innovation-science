function clickRedirectHandler(event) {
  $(document).on('click', 'a', function(evt) {
    var result = confirm('Sure?')
    return result;
  })
}
  
function setRedirectConfirmationDialogs() {
  var result2 = clickRedirectHandler();
  if ( result2 == true ) {
    var redirect=$(this).find('a').attr('href');
    window.location.replace(redirect);
  }
}

setRedirectConfirmationDialogs()



