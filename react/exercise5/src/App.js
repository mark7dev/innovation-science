import React, { Component } from 'react';
import './App.css';

class App extends Component {

  constructor() {
    super();

    this.state = {
      firstName: "",
      age: "",
      email: ""
    }

  }

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
   }

   verifyForm = (e) => {
    e.preventDefault();
    console.log(this.state.firstName);
    console.log(this.state.age);
    console.log(this.state.email);
  }

  render() {
    return (
      <div className="App">
          <form>
            <input name="firstName" type="text" placeholder="First Name" onChange={this.handleChange}></input>
            <input name="age" type="number" placeholder="Age" onChange={this.handleChange}></input>
            {
              this.state.age>=14 ? 
              <input name="email" type="email" placeholder="Email" onChange={this.handleChange}></input>
              : ""
            }            
            <button onClick={this.verifyForm}>Submit</button>
          </form>
      </div>
    );
  }
}

export default App;
