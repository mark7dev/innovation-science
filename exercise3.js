function update(increase) {
    
    if(increase == 50) {
        document.getElementById('loading-bar').value = increase;
        return false;
    }
    if(increase == 100) {
        document.getElementById('loading-bar').value = increase;
        return true;
    }
    
  }
  
  // Example case. 
  document.body.innerHTML = `<progress id="loading-bar" value="0" max="100"></progress>`;
  console.log(update(50)); // should return false and loading-bar's value should be 50.
  console.log(update(100)); // should return true and loading-bar's value should be 100.
  
